# DKM Project

## Dataset

The dataset is supposed to be located in the `./Data/2017/` folder.

## How to run

```
python3 parser.py
```

A file named `accidents.ttl` is then generated.
