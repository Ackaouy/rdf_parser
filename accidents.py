from rdflib import Graph, URIRef, Literal, BNode, RDF, Namespace
from rdflib.namespace import RDF, RDFS, FOAF, XSD
from datetime import datetime

# Prefixes
dbp = Namespace("http://dbpedia.org/property/")
dbc = Namespace("http://dbpedia.org/ontology/classes#")
geo = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
ex = Namespace("http://www.example.org/dataAccident#")
exrd = Namespace("http://www.example.org/dataAccident/Road#")
igeo = Namespace("http://id.insee.fr/def/geo#")
iigeo = Namespace("http://id.insee.fr/geo/commune/")
xsd = Namespace("http://www.w3.org/2001/XMLSchema#")



'''
    Fichier avec toutes les classes.

    Une classe correspond à un nœud, chaque nœud à plusieurs attributs définis par les variables. Une variable peut
    aussi pointer vers un autres nœud. Par exemple toutes les variables de la classe/du noœud Accident pointent vers d'autres
    classes/nœuds.
'''

class Accident:

    def __init__(self, acc_id, date, conditions, localisation):

        self.date = date
        self.localisation = localisation
        self.conditions = conditions
        self.personnes = []
        self.acc_id = acc_id
        self.done = False

    def setRoute(self, route):

        self.route = route

    def addPersonne(self, personne):

        self.personnes.append(personne)

    def addToRDF(self, store):

        if not self.done:
          acc = URIRef(ex + str(self.acc_id))
          com = URIRef(iigeo + self.localisation.getCodeINSEE())
          store.add((acc, RDF.type, ex.RoadAccident))
          store.add((acc, geo.long, Literal(self.localisation.long, datatype=XSD.float)))
          store.add((acc, geo.lat, Literal(self.localisation.lat, datatype=XSD.float)))
          store.add((acc, ex.Commune, com))
          store.add((com, RDF.type, igeo.Commune))
          store.add((acc, ex.date_time, Literal(self.date.toString(), datatype=XSD.dateTime)))
          if self.conditions.Intersection() != None : store.add((acc, ex.intersection, Literal(self.conditions.Intersection(), datatype=XSD.string)))
          if self.conditions.Lumiere() != None : store.add((acc, ex.light, Literal(self.conditions.Lumiere(), datatype=XSD.string)))
          if self.conditions.Meteo() != None : store.add((acc, ex.weather, Literal(self.conditions.Meteo(), datatype=XSD.string)))
          try:
            road = URIRef(exrd + str(self.route.voie))
            store.add((acc, dbp.location, road ))
            store.add((road, RDF.type, dbc.Road))
            self.route.addToRDF(store, acc)
          except AttributeError:
            pass
          for p in self.personnes:
            pers = BNode()
            store.add((acc, ex.involves, pers))
            p.addToRDF(store, pers)
          self.done = True

class Date:

    def __init__(self, an, mois, jour, heure):

        self.an = int(an)
        self.mois = int(mois)
        self.jour = int(jour)
        self.heure = int(heure)

    def toString(self):
      try:
        hour = int(str(self.heure)[0:len(str(self.heure))-2])
      except ValueError:
        hour = 0
      minute = int(str(self.heure)[-2:])
      d = datetime(year=self.an + 2000, month=self.mois, day=self.jour, hour=hour, minute=minute)
      d = d.strftime("%Y-%m-%dT%H:%M:%S")
      return str(d)


class Conditions:

  lum = {
    0: None,
    1: "Plein jour",
    2: "Crépuscule ou aube",
    3: "Nuit sans éclairage public",
    4: "Nuit avec éclairage public non allumé",
    5: "Nuit avec éclairage public allumé",
  }
  inter = {
    0: None,
    1: "Hors intersection",
    2: "Intersection en X",
    3: "Intersection en T",
    4: "Intersection en Y",
    5: "Intersection à plus de 4 branches",
    6: "Giratoire",
    7: "Place",
    8: "Passage à niveau",
    9: "Autre intersection",
  }
  met = {
    0: None,
    1: "Normale",
    2: "Pluie légère",
    3: "Pluie forte",
    4: "Neige - grêle",
    5: "Brouillard - fumée",
    6: "Vent fort - tempête",
    7: "Temps éblouissant",
    8: "Temps couvert",
    9: "Autre",
  }

  def __init__(self, intersection, lumiere, meteo):

    self.intersection = int(intersection)
    self.lumiere = int(lumiere)
    self.meteo = int(meteo)

  def Intersection(self):

    return self.inter[self.intersection]

  def Lumiere(self):

    return self.lum[self.lumiere]

  def Meteo(self):

    return self.met[self.meteo]


class Localisation:

    def __init__(self, lat, long, commune, dep):
      self.lat = float(lat/1e5)
      self.long = float(long/1e5)
      self.commune = str(commune)
      self.dep = str(dep)

    def getCodeINSEE(self):
      return self.dep[0:2] + self.commune

class Route:

  categories = {
    0: None,
    1: "Autoroute",
    2: "Route Nationale",
    3: "Route Départementale",
    4: "Voie Communale",
    5: "Hors réseau public",
    6: "Parc de stationnement ouvert à la circulation publique",
    9: "Autre",
  }
  plans = {
    0: None,
    1: "Partie rectiligne",
    2: "En courbe à gauche",
    3: "En courbe à droite",
    4: "En S",
  }
  profiles = {
    0: None,
    1: "Plat",
    2: "Pente",
    3: "Sommet de côte",
    4: "Bas de côté",
  }
  infrastructures = {
    0: None,
    1: "Souterrain - tunnel",
    2: "Pont - autopont",
    3: "Bretelle d’échangeur ou de raccordement",
    4: "Voie ferrée",
    5: "Carrefour aménagé",
    6: "Zone piétonne",
    7: "Zone de péage",
  }
  circulations = {
    0: None,
    1: "A sens unique",
    2: "Bidirectionnelle",
    3: "A chaussées séparées",
    4: "Avec voies d’affectation variable",
  }

  def __init__(self, voie, cat_route, nmbr_voies, larg_chaussee, larg_terre_plein, plan, profil, infrastructure, circulation):

    self.voie = int(voie)
    self.cat_route = int(cat_route)
    self.nmbr_voies = int(nmbr_voies)
    self.larg_chaussee = float(larg_chaussee)
    self.larg_terre_plein = float(larg_terre_plein)
    self.plan = int(plan)
    self.profil = int(profil)
    self.infrastructure = int(infrastructure)
    self.circulation = int(circulation)

  def addToRDF(self, store, acc):

    road = URIRef(exrd + str(self.voie))
    store.add((road, RDF.type, dbc.Road))
    if self.categories[self.cat_route] != None : store.add((acc, ex.catr, Literal(self.categories[self.cat_route], datatype=XSD.string)))
    store.add((acc, dbp.numberOfLanes, Literal(self.nmbr_voies, datatype=XSD.integer)))
    store.add((acc, dbp.trackWidth, Literal(self.larg_chaussee, datatype=XSD.double)))
    store.add((acc, ex.centralWidth, Literal(self.larg_terre_plein, datatype=XSD.double)))
    if self.plans[self.plan] != None : store.add((acc, ex.layout, Literal(self.plans[self.plan], datatype=XSD.string)))
    if self.profiles[self.profil] != None : store.add((acc, ex.profile, Literal(self.profiles[self.profil], datatype=XSD.string)))
    #store.add((road, ex.infrastructure, Literal(self.infrastructures[self.infrastructure], datatype=XSD.string)))
    if self.circulations[self.circulation] != None : store.add((acc, ex.circulation, Literal(self.circulations[self.circulation], datatype=XSD.string)))

class Personne:

  grav = {
    0: None,
    1: "Indemne",
    2: "Tué",
    3: "Blessé hospitalisé",
    4: "Blessé léger",
  }
  sex = {
    0: None,
    1: "Masculin",
    2: "Féminin",
  }
  actions = {
    0: None,
    1: "Sens véhicule heurtant",
    2: "Sens inverse du véhicule",
    3: "Traversant",
    4: "Masqué",
    5: "Jouant – courant",
    6: "Avec animal",
    9: "Autre",
  }
  secuE = {
    0: None,
    1: "Ceinture",
    2: "Casque",
    3: "Dispositif enfants",
    4: "Equipement réfléchissant",
    9: "Autre",
  }
  secuU = {
    0: None,
    1: "Oui",
    2: "Non",
    3: "Non déterminable",
  }
  loca = {
    0: None,
    1: "Sens véhicule heurtant",
    2: "Sens inverse du véhicule",
    3: "Traversant",
    4: "Masqué",
    5: "Jouant – courant",
    6: "Avec animal",
    9: "Autre",
  }
  cats = {
    0: None,
    1: "Conducteur",
    2: "Passager",
    3: "Piéton",
    4: "Piéton en roller ou en trottinette",
  }
  reasons = {
    0: None,
    1: "Domicile – travail",
    2: "Domicile – école",
    3: "Courses – achats",
    4: "Utilisation professionnelle",
    5: "Promenade – loisirs",
    9: "Autre",
  }

  def __init__(self, gravite, annee_de_naissance, sexe, action_pieton, existence_eqpt_secu,
               utilisation_eqpt_secu, localisation, categorie_usager, motif_trajet,
               place, vehicule):

    self.gravite = int(gravite)
    self.annee_de_naissance = int(annee_de_naissance)
    self.sexe = int(sexe)
    self.action_pieton = int(action_pieton)
    self.existence_eqpt_secu = int(existence_eqpt_secu)
    self.utilisation_eqpt_secu = int(utilisation_eqpt_secu)
    self.localisation = int(localisation)
    self.categorie_usager = int(categorie_usager)
    self.motif_trajet = int(motif_trajet)
    self.place = int(place)
    self.vehicule = vehicule
    self.done = False

  def addToRDF(self, store, pers):

    if not self.done:
      store.add((pers, RDF.type, FOAF.Person))
      if self.grav[self.gravite] != None : store.add((pers, ex.gravity, Literal(self.grav[self.gravite], datatype=XSD.string)))
      store.add((pers, ex.birthYear, Literal(self.annee_de_naissance, datatype=XSD.integer)))
      if self.sex[self.sexe] != None : store.add((pers, FOAF.gender, Literal(self.sex[self.sexe], datatype=XSD.string)))
      if self.actions[self.action_pieton] != None : store.add((pers, ex.action, Literal(self.actions[self.action_pieton], datatype=XSD.string)))
      if self.secuE[self.existence_eqpt_secu] != None : store.add((pers, ex.securityExistence, Literal(self.secuE[self.existence_eqpt_secu], datatype=XSD.string)))
      if self.secuU[self.utilisation_eqpt_secu] != None : store.add((pers, ex.securityUse, Literal(self.secuU[self.utilisation_eqpt_secu], datatype=XSD.string)))
      if self.loca[self.localisation] != None : store.add((pers, ex.localisation, Literal(self.loca[self.localisation], datatype=XSD.string)))
      if self.cats[self.categorie_usager] != None : store.add((pers, ex.category, Literal(self.cats[self.categorie_usager], datatype=XSD.string)))
      if self.reasons[self.motif_trajet] != None : store.add((pers, ex.reason, Literal(self.reasons[self.motif_trajet], datatype=XSD.string)))
      #store.add((pers, ex.place, Literal(self.place, datatype=XSD.integer)))
      if self.vehicule != None:
        veh = BNode()
        store.add((pers, ex.vehicule, veh))
        self.vehicule.addToRDF(store, veh)
      self.done = True

class Vehicule:

  senses = {
    0: None,
    1: "PK ou PR ou numéro d’adresse postale croissant",
    2: "PK ou PR ou numéro d’adresse postale décroissant",
  }
  cats = {
    0: None,
    1: "Bicyclette",
    2: "Cyclomoteur <50cm3",
    3: "Voiturette (Quadricycle à moteur carrossé) (anciennement voiturette ou tricycle à moteur)",
    4: "Référence plus utilisée depuis 2006 (scooter immatriculé)",
    5: "Référence plus utilisée depuis 2006 (motocyclette)",
    6: "Référence plus utilisée depuis 2006 (side-car)",
    7: "VL seul",
    8: "Catégorie plus utilisée (VL + caravane)",
    9: "Catégorie plus utilisée (VL + remorque)",
    10: "VU seul 1,5T <= PTAC <= 3,5T avec ou sans remorque (anciennement VU seul 1,5T <= PTAC <= 3,5T)",
    11: "Référence plus utilisée depuis 2006 (VU (10) + caravane)",
    12: "Référence plus utilisée depuis 2006 (VU (10) + remorque)",
    13: "PL seul 3,5T <PTCA <= 7,5T",
    14: "PL seul > 7,5T",
    15: "PL > 3,5T + remorque",
    16: "Tracteur routier seul",
    17: "Tracteur routier + semi-remorque",
    18: "Référence plus utilisée depuis 2006 (transport en commun)",
    19: "Référence plus utilisée depuis 2006 (tramway)",
    20: "Engin spécial",
    21: "Tracteur agricole",
    30: "Scooter < 50 cm3",
    31: "Motocyclette > 50 cm 3 et <= 125 cm 3",
    32: "Scooter > 50 cm 3 et <= 125 cm 3",
    33: "Motocyclette > 125 cm 3",
    34: "Scooter > 125 cm 3",
    35: "Quad léger <= 50 cm 3 (Quadricycle à moteur non carrossé)",
    36: "Quad lourd > 50 cm 3 (Quadricycle à moteur non carrossé)",
    37: "Autobus",
    38: "Autocar",
    39: "Train",
    40: "Tramway",
    99: "Autre véhicule",
  }
  obsfix = {
    0: None,
    1: "Véhicule en stationnement",
    2: "Arbre",
    3: "Glissière métallique",
    4: "Glissière béton",
    5: "Autre glissière",
    6: "Bâtiment, mur, pile de pont",
    7: "Support de signalisation verticale ou poste d’appel d’urgence",
    8: "Poteau",
    9: "Mobilier urbain",
    10: "Parapet",
    11: "Ilot, refuge, borne haute",
    12: "Bordure de trottoir",
    13: "Fossé, talus, paroi rocheuse",
    14: "Autre obstacle fixe sur chaussée",
    15: "Autre obstacle fixe sur trottoir ou accotement",
    16: "Sortie de chaussée sans obstacle",
  }
  obsmob = {
    0: None,
    1: "Piéton",
    2: "Véhicule",
    4: "Véhicule sur rail",
    5: "Animal domestique",
    6: "Animal sauvage",
    9: "Autre",
  }
  chocs = {
    0: None,
    1: "Avant",
    2: "Avant droit",
    3: "Avant gauche",
    4: "Arrière",
    5: "Arrière droit",
    6: "Arrière gauche",
    7: "Côté droit",
    8: "Côté gauche",
    9: "Chocs multiples (tonneaux)",
  }
  mans = {
    0: None,
    1: "Sans changement de direction",
    2: "Même sens, même file",
    3: "Entre 2 files",
    4: "En marche arrière",
    5: "A contresens",
    6: "En franchissant le terre-plein central",
    7: "Dans le couloir bus, dans le même sens",
    8: "Dans le couloir bus, dans le sens inverse",
    9: "En s’insérant",
    10: "En faisant demi-tour sur la chaussée",
    11: "Changeant de file - A gauche",
    12: "Changeant de file - A droite",
    13: "Déporté - A gauche",
    14: "Déporté - A droite",
    15: "Tournant- A gauche",
    16: "Tournant - A droite",
    17: "Dépassant - A gauche",
    18: "Dépassant - A droite",
    19: "Traversant la chaussée",
    20: "Manœuvre de stationnement",
    21: "Manœuvre d’évitement",
    22: "Ouverture de porte",
    23: "Arrêté (hors stationnement)",
    24: "En stationnement (avec occupants)",
  }

  def __init__(self, acc_id, veh_id, sens, categorie, obstacle_fixe, obstacle_mobile,
               point_de_choc, manoeuvre):

    self.acc_id = acc_id
    self.veh_id = veh_id
    self.sens = int(sens)
    self.categorie = int(categorie)
    self.obstacle_fixe = int(obstacle_fixe)
    self.obstacle_mobile = int(obstacle_mobile)
    self.point_de_choc = int(point_de_choc)
    self.manoeuvre = int(manoeuvre)
    self.done = False

  def addToRDF(self, store, veh):

    if not self.done:
      store.add((veh, RDF.type, ex.Vehicule))
      if self.senses[self.sens] != None : store.add((veh, ex.sens, Literal(self.senses[self.sens], datatype=XSD.string)))
      if self.cats[self.categorie] != None : store.add((veh, ex.categoryV, Literal(self.cats[self.categorie], datatype=XSD.string)))
      if self.obsfix[self.obstacle_fixe] != None : store.add((veh, ex.obstacleFixed, Literal(self.obsfix[self.obstacle_fixe], datatype=XSD.string)))
      if self.obsmob[self.obstacle_mobile] != None : store.add((veh, ex.obstacleMobile, Literal(self.obsmob[self.obstacle_mobile], datatype=XSD.string)))
      if self.chocs[self.point_de_choc] != None : store.add((veh, ex.impact, Literal(self.chocs[self.point_de_choc], datatype=XSD.string)))
      if self.mans[self.manoeuvre] != None : store.add((veh, ex.maneuver, Literal(self.mans[self.manoeuvre], datatype=XSD.string)))
      self.done = True
