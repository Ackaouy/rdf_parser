\documentclass[10pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{minted}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{todonotes}
\usepackage{placeins}
\usepackage{subcaption}

\title{\textsc{M2 SIF - DKM Project} \\ From Data Modeling to Data Exploration}
\author{Antoine Ackaouy, Cedric De Almeida Braga, Florestan De Moor}
\date{November 5th, 2018}

\begin{document}

\maketitle

\section{Introduction}
According to the W3C, semantic web aims at providing a framework in order to share data across all kinds of boundaries. To achieve this, data has to be represented according a model, which is RDF, and accessible through a query language which is SPARQL.
The goal of this project is to manipulate the Semantic Web technologies.
We first select a dataset and build a RDF model out of it, which then allows interactive exploration and querying using the SPARQL language to extract relevant information.

\section{Data Collection}

We consider a dataset related to road accidents, available on the open data website of the French government\footnote{\url{https://www.data.gouv.fr/fr/datasets/base-de-donnees-accidents-corporels-de-la-circulation}}.
The dataset is composed of four coma-separated files which give a description of each accident that occurred in 2017 in France, along with information about the persons involved, their vehicles and the location.
A fifth file is also provided to explain the format used and the meaning of each value through dictionaries since most string attributes were mapped to integers in the dataset.

Our goal is to analyze this dataset in order to determine correlations and answer relevant questions one might ask, such as the most deadly roads or the frequency of accidents with respects to the age of the driver.

\section{RDF Modeling}

In order to create a RDF from the data, we created a RDF Schema with the class that will be used and their properties. Once the structure was set, we used python to automate the conversion in turtle format. From our data, we set 5 classes to be used in our modelling, each with a set of properties. Whenever it was possible, we used already existing ontologies to define properties and classes. But in most of the cases, they were not referenced. In this case, we used a dummy namespace to create URIs, with the prefix ex:.

The 5 classes are RoadAccident,  Road, Vehicle, Commune and Person. While each RoadAccident is at the center of the graph, the other 5 are linked to it via different properties (\figurename~\ref{fig:visu}). 

\begin{figure}[!h]
	\centering
	
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=\textwidth]{images/acc-1.png}
		\caption{Accident node (1/2)}
		\vspace*{2em}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=\textwidth]{images/acc-2.png}
		\caption{Accident node (2/2)}
		\vspace*{2em}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=\textwidth]{images/visu-pers-2.png}
		\caption{Person blank node}
		\vspace*{2em}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=\textwidth]{images/visu-veh-3.png}
		\caption{Vehicule blank node}
		\vspace*{2em}
	\end{subfigure}
	
	\caption{Graphical visualization of the RDF of a road accident}
	\label{fig:visu}
\end{figure}

The RoadAccidents are set as a subclass of a DBpedia class (dbc:) Event and identified by an URI and Vehicule is a subclass of dbc:MeanOfTransportation, in order to get some interesting properties from DBpedia. We also used the class dbc:Road, foaf:Person and the class Commune from the INSEE database in order to have the possibility  to expand the dataset in the future.

As the persons involved in the accidents are not identified in the data in a unique way, they are represented by a blank node. The same goes for the vehicles. The location properties of each accident are also attached to it via a blank node, along with the instance of the class road that is concerned. This is due to the fact that for a given road, different set of features given the exact place of the accident.


\subsection{RDF conversion}

In a first place, we will define the different prefixes we will be using in \figurename~\ref{pref}.
\begin{figure}[h]
	\begin{minted}[frame=lines]{sparql}
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX ex: <http://www.example.org/dataAccident#>
PREFIX igeo: <http://id.insee.fr/def/geo#>
PREFIX idemo: <http://rdf.insee.fr/def/demo#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dbc: <http://dbpedia.org/ontology/classes#>
PREFIX dbp: <http://dbpedia.org/property/>
\end{minted}
\caption{RDF prefixes}\label{pref}
\end{figure}

We write a parser for the dataset in Python and use the RDFlib\footnote{\url{https://github.com/RDFLib/rdflib}} to build the RDF graph.
Once done, the library allows us the serialize the graph into a Turtle formatted file, which we can then import in a RDF store.
Our parser checks that the data read is well-formatted and simply ignores triples that are not.
For instance, we encountered cases where the parser found a \texttt{NaN} value while an integer was expected.
An interesting aspect of the dataset is that it is divided into four different files, which can be joined together through a primary key (for accidents) or a foreign key (for vehicles).
These files can be processed separately into four RDF graphs that can simply be merged in the end by concatenating the Turtle files.
We also explicitly give the RDFS schema in the graph, which is useful information when working with the SPARKLIS exploration tool as we explain later.

\section{SPARQL Querying}

Now that we have fit our data in a RDF model, it becomes far more easier to interpret it. SPARQL - SPARQL Protocol And RDF Query Language - is a protocol and query language aiming to deal with large RDF datasets. It was at first created for the Semantic Web but was found to be useful in many other domains where the processing of huge amount of data was necessary. By following the graph format of RDF, SPARQL queries are based on triples.

In this section we propose examples of interesting queries on our RDF data. 

\subsection{Birth year}

This first query aims to search for a relation between the birth year and the involvement in an accident, it's shown in Figure.\ref{s-bir}
\begin{figure}[h]
	\begin{minted}[frame=lines, fontsize=\small]{sparql}
SELECT DISTINCT ?birthyear 
(COUNT(DISTINCT ?x) AS ?birthyearaccident)
WHERE { ?x ex:birthYear ?date.
        BIND (?date AS ?birthyear) }
GROUP BY ?birthyear
ORDER BY ?birthyear
\end{minted}
\caption{SPARQL query for the impact of birth year on accidents}\label{s-bir}
\end{figure}

Here, we select \mintinline{sparql}|?x| the persons which have a birth year of \mintinline{sparql}|?date|, we aggregate all those dates in the variable \mintinline{sparql}|?birthyear|, we count the \mintinline{sparql}|?x| and group them by \mintinline{sparql}|?birthyear|. The results is given in \figurename~\ref{fig:birth}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=7cm]{images/Birth.png}
		\caption{The number of accidents per birth year}
		\label{fig:birth}
\end{figure}	

We clearly see that teenagers who just had their driving licences have more accidents than older people.

\FloatBarrier

\subsection{Impact of the gender on accidents}

It's well known that car insurances are more expensive for men than for women. We therefore asked ourselves wether this price difference was founded. This query aims to compare the number of accidents involving men as driver to the number of accidents involving women as driver, it's shown in \figurename~\ref{s-gend}.

\begin{figure}[!h]
	\begin{minted}[frame=lines, fontsize=\small]{sparql}
SELECT ?genders (COUNT(DISTINCT(?y)) as ?numberOfInvolved) 
WHERE {?x a ex:RoadAccident.
  ?x ex:involves ?y.
  ?y foaf:gender ?genders;
     ex:category "Conducteur".}
GROUP BY ?genders
\end{minted}
\caption{SPARQL query for the impact of genders on accidents}\label{s-gend}
\end{figure}

Here, we select the road accident \mintinline{sparql}|?x| that involves persons \mintinline{sparql}|?y| whose \mintinline{sparql}|ex:category| is "Conducteur" - the french word for driver. We group those person by genders and count the number of person involved for each gender. The result is showed in \figurename~\ref{fig:gender}.

\begin{figure}[h]
	\centering
	\includegraphics[width=9cm]{images/Genders.png}
		\caption{The number of accidents with regard to the driver's gender}
		\label{fig:gender}
\end{figure}	

We can see that there are far more accidents involving men than women, yet here, we didn't take into account the number of person actively driving per gender. There is maybe more men driving than there is women, nevertheless it is very unlikely that this bias explains the huge difference.

\subsection{What is the most dangerous hour ?}

\paragraph{}In this query we will try to figure out what are the hours where most of the accidents occur: Figure \ref{s-h}.
\begin{figure}[h]
	\begin{minted}[frame=lines, fontsize=\small]{sparql}
SELECT ?hours (COUNT(DISTINCT ?x ) as ?accidents) 
WHERE {?x a ex:RoadAccident.
  ?x ex:date_time ?date
  BIND(hours(?date) as ?hours)}
GROUP BY ?hours
ORDER BY desc(?accidents)
\end{minted}
\caption{SPARQL query for accidents per hour}\label{s-h}
\end{figure}
 
Here, we select the \mintinline{sparql}|?x| road accidents having a date \mintinline{sparql}|?date|. We group the results by hours \mintinline{sparql}|?hours|  and count the distinct accidents. The result is given in \figurename~\ref{fig:hours}.

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{images/Hours.png}
		\caption{The number of accidents for the different hours}
		\label{fig:hours}
\end{figure}	

\FloatBarrier

\subsection{What is the most "dangerous" vehicule}

It would interesting to see wether a vehicle is more involved in lethal accident Figure \ref{s-cat}.
 \begin{figure}[!h]
 	\begin{minted}[frame=lines, fontsize=\small]{sparql}
SELECT ?cat ?ratio
WHERE {{
SELECT (COUNT(DISTINCT ?x) as ?killed_accidents) ?cat
  WHERE {?x a ex:RoadAccident.
  	?x ex:involves ?y.
  	?y ex:vehicule ?vehicule;
       ex:gravity "Tué".
    ?vehicule ex:categoryV ?cat}
GROUP by ?cat}
{SELECT (COUNT(DISTINCT ?y) as ?total_accidents) ?cat
WHERE {?y a ex:Vehicule.
    ?y ex:categoryV ?cat.}
GROUP by ?cat}
  BIND((?killed_accidents/?total_accidents) as ?ratio)}
\end{minted}
\caption{SPARQL query for the ratio of killed by vehicle category}\label{s-cat}
 \end{figure}
 
 This request requires two levels, one which will select the number of accidents for which the users were killed and the number of accidents per category of vehicle. The second level allows us to compute the ratio of lethal accidents and total accidents. The result is showed in \figurename~\ref{fig:ratio} while the categories of the vehicles are given in \tablename~\ref{tab:index}.
 
\begin{figure}
	\centering
	\includegraphics[width=10cm]{images/Cat.png}
		\caption{The ratio of lethal accidents per category}
		\label{fig:ratio}
\end{figure}

We can see 4 types of vehicles having a high ratio of lethal accident: heavy quads, light quads, carts and motorcycle. Theses are probably types of vehicles for which the driver's behavior is the most dangerous. On the other hand, heavyweight vehicles accidents are less likely to kill their users. 

\FloatBarrier

\subsection{Ratio of accidents per inhabitants}

The last request we will explore tries to find the cities in which we are the more likely to get an accident. We will only focus on cities with more than 10000 inhabitants . Otherwise, cities with few inhabitants and a small number of accidents always resulted in the first places with really high ratios. The query is as follow is shown in \figurename~\ref{ratio_habs}.

\begin{figure}[h]
	\begin{minted}[frame=lines, fontsize=\small]{sparql}
SELECT ?nom ?ratio
WHERE{
{SELECT ?nom (COUNT(DISTINCT ?x) as ?accidents) ?commune
    WHERE {?x a ex:RoadAccident;
      ex:Commune ?commune.
      ?commune igeo:nom ?nom.}
GROUP BY ?nom ?commune}
  {SELECT ?pop ?commune
    WHERE {?commune idemo:population ?p.
      ?p idemo:populationTotale ?pop.
      FILTER(?pop > 10000)}
    GROUP by ?commune ?population}
  BIND((?accidents/?population) as ?ratio)}
ORDER by desc(?ratio)

	\end{minted}
	\caption{SPARQL Query to get the ratio of accidents per inhabitants}\label{ratio_habs}
\end{figure} 

In this query we used the RDF database of the INSEE to get the population and names of the cities. In this query we first select all the \mintinline{sparql}|?accidents| for a given \mintinline{sparql}|?commune| and we recover the \mintinline{sparql}|?nom| of this given \mintinline{sparql}|?commune|. Then in a second \mintinline{sparql}|SELECT|, we fetch the \mintinline{sparql}|?population| of the same \mintinline{sparql}|?commune|. To retrieve only the city with more than 10000 inhabitants, we use the  \mintinline{sparql}|FILTER| function.Finally we compute by using \mintinline{sparql}|BIND| the ration between the \mintinline{sparql}|?accidents| and the \mintinline{sparql}|?population|. The results are given in \figurename~\ref{pop_ratio}.

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{images/Pop}
	\caption{Ratio of accidents per inhabitants}\label{pop_ratio}
\end{figure}

The city with the highest ratio - far above the other cities - is Cayenne. This is a small and very touristic city in Guyanne. This could explain the high ratio of accidents, more than computing the ratio per inhabitants it would be more significative to compute the ratio per visitors. We also added the city of Rennes in the plot to see it's ratio. In a SPARQL query, it is also simple to get this particular value. Rather than selecting all the \mintinline{sparql}|?commune| with a given \mintinline{sparql}|?nom|, we just have to  fetch the one with the name "Rennes": \mintinline{sparql}|?commune igeo:nom "Rennes"|.

\begin{table}
	\centering
	\scriptsize
	\begin{tabular}{@{}p{0.1\textwidth}p{0.9\textwidth}@{}}
	\toprule
	Index & Vehicle categories \\
	\midrule
	1 & Cyclomoteur $<$50 cm$\!^3$ \\
	2 & PL seul 3.5T $<$PTCA $\le$ 7.5T\\
	3 & Tracteur routier + semi-remorque \\
	4 & Tracteur agricole \\
	5 & Quad lourd $>$ 50 cm$\!^3$ \\
	6 & Autre vehicule \\
	7 & Bicyclette \\
	8 &  PL $>$ 3.5T + remorque \\
	9 &  VU seul 1.5T $\le$ PTAC $\le$ 3.5T avec ou sans remorque  \\
	10 & Motocyclette $>$ 125 cm$\!^3$ \\
	11 & Engin special \\
	12 & PL seul $>$ 7.5T \\
	13 & VL seul \\
	14 & Scooter $<$ 50 cm$\!^3$ \\
	15 & Motocyclette $>$ 50 cm$\!^3$ et $\le$ 125 cm$\!^3$ \\
	16 & Voiturette (Quadricycle) (anciennement voiturette ou tricycle a moteur)\\
	17 & Scooter $>$ 125 cm$\!^3$\\
	18 & Quad leger $\le$ 50 cm$\!^3$ (Quadricycle a moteur non carrossé) \\
	19 & Scooter $>$ 50 cm$\!^3$ et $\le$ 125 cm$\!^3$ \\
	\bottomrule
	\end{tabular}
	\caption{Table referencing categories of vehicles}
	\label{tab:index}
\end{table}

\FloatBarrier

\section{Exploration}

Sparklis is a powerful tool that ables to explore RDF graphs with ease. The aim is to select queries using natural language. For example the SPARQL query : 
 \begin{minted}[frame=lines, fontsize=\small]{sparql}
?person a foaf:person;
          foaf:name "Julien"	
\end{minted}
Comes down to "Give me every person whose name is 'Julien' ".

In this section we will use Sparklis to compute more complex SPARQL requests.

\subsection{Effect of lightning on accident seriousness}

One could think that driving in dark environment is more dangerous. In this request we will try to figure it out \figurename~\ref{S_lights}.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/Sparklis_light.png}
	\caption{Sparklis request}\label{S_lights}
\end{figure}

Wfe select the two things: the number of accidents per lights conditions and gravity and the number of accidents per lights conditions. We then divide those two numbers to obtain the ratio of each gravities per lights accidents. The result is shown in \figurename~\ref{G_lights}.

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{images/Lights.png}
	\caption{Ratio of accidents per gravity and lights conditions}\label{G_lights}
\end{figure}

The lowest ratio is - fortunately - for the lethal accident. However the probability of a lethal accident is almost twice as important by night without lights than in other lights conditions. Furthermore, when no lights are turned on and an accident occurs it's more likely to get hurt and to go to the hospital than to be unscathed.

\FloatBarrier

\subsection{Using the map}

One great feature of sparklis is it's ability to directly link GPS coordinates to a map. In this query, we selected the accidents which happened in Lens and plot them on the map. The sparklis query is shown in \figurename~\ref{spark-m}.

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{images/Sark_map.png}
	\caption{Sparklis request}\label{spark-m}
\end{figure}
The resulting map is shown in Figure \ref{map} 
\begin{figure}[h]
	\centering
	\includegraphics[width=16cm]{images/map}
	\caption{Resulting map}\label{map}
\end{figure}

\FloatBarrier

\section{Conclusion}

In this project, we used the dataset of the French government on accidents to show the power of RDF databases.
Thanks to RDF, we could easily join this dataset with the one from the INSEE. This allowed us to easily make some interesting requests using the SPARQL language.
Finally, we used the tool Sparklis which allows to easily make some requests and explore our dataset.

\end{document}