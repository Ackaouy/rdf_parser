from rdflib import Graph, URIRef, Literal, BNode, RDF, Namespace
from rdflib.namespace import XSD

store = Graph()

# Prefix
ex = Namespace("http://example.org/")

# Fill graph
i1 = BNode()
a1 = BNode()
i2 = BNode()
a2 = BNode()
store.add((i1, RDF.value, ex.greenMango))
store.add((i1, ex.amount, a1))
store.add((a1, RDF.value, Literal('1', datatype=XSD.integer)))
store.add((a1, ex.unit, ex.lb))
store.add((i2, RDF.value, ex.CayennePepper))
store.add((i2, ex.amount, a2))
store.add((a2, RDF.value, Literal('7.5', datatype=XSD.decimal)))
store.add((a2, ex.unit, ex.tsp))
store.add((ex.Chutney, ex.hasIngredient, i1))
store.add((ex.Chutney, ex.hasIngredient, i2))

# Serialize as Turtle and write in a .ttl file
with open("test.ttl", 'w') as f:
	f.write(store.serialize(format="turtle").decode("ascii"))
