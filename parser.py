import pandas as pd
from accidents import *
from rdflib import Graph
from rdflib.namespace import RDF, RDFS, FOAF, XSD, NamespaceManager


dbp = Namespace("http://dbpedia.org/property/")
dbc = Namespace("http://dbpedia.org/ontology/classes#")
geo = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
ex = Namespace("http://www.example.org/dataAccident#")
igeo = Namespace("http://id.insee.fr/def/geo#")
iigeo = Namespace("http://id.insee.fr/geo/")

datadir = "./Data/2017/"


'''
  Fonction pour importer les données, et créer les classes.
  Il ne reste plus qu'à itérer et à partir des données créer le fichier RDF.
'''

def create_classes():

  accidents = {}
  vehicules = {}

  #On importe les données sous la forme d'un data_frame Pandas en utilisant le ; comme séparateur et l'encodage utf-8.

  table = pd.read_csv(datadir + "caracteristiques-2017.csv", sep=",", encoding="utf-8")
  for i in range(len(table)):
    I = len(table)
    if i >= I:
      break

    try:
      x = table.loc[i]
      acc_id = x["Num_Acc"]
      date = Date(an=x["an"], mois=x["mois"], jour=x["jour"], heure=x["hrmn"])
      conditions = Conditions(intersection=x["int"], lumiere=x["lum"], meteo=x["atm"])
      localisation = Localisation(lat=x["lat"], long=x["long"], commune=x["com"], dep=x["dep"])
      accident = Accident(acc_id=acc_id, date=date, conditions=conditions, localisation=localisation)
      accidents[acc_id] = accident
      vehicules[acc_id] = {}
    except ValueError:
      continue

  table = pd.read_csv(datadir + "lieux-2017.csv", sep=",", encoding="utf-8")
  for i in range(len(table)):

    if i >= I:
      break

    try:
      x = table.loc[i]
      acc_id = x["Num_Acc"]
      route = Route(voie=x["voie"], cat_route=x["catr"], nmbr_voies=x["nbv"],
                     larg_chaussee=x["larrout"], larg_terre_plein=x["lartpc"],
                     plan=x["plan"], profil=x["prof"], infrastructure=x["infra"], circulation=x["circ"])
      accidents[acc_id].setRoute(route)
    except ValueError:
      continue
    except KeyError:
      continue

  table = pd.read_csv(datadir + "vehicules-2017.csv", sep=",", encoding="utf-8")
  for i in range(len(table)):

    if i >= I:
      break

    try:
      x = table.loc[i]
      acc_id = x["Num_Acc"]
      veh_id = x["num_veh"]
      vehicule = Vehicule(acc_id=acc_id, veh_id=veh_id, sens=x["senc"],
                          categorie=x["catv"], obstacle_fixe=x["obs"],
                          obstacle_mobile=x["obsm"], point_de_choc=x["choc"],
                          manoeuvre=x["manv"])
    except ValueError:
      continue

    vehicules[acc_id][veh_id] = vehicule

  table = pd.read_csv(datadir + "usagers-2017.csv", sep=",", encoding="utf-8")
  for i in range(len(table)):

    if i >= I:
      break

    try:
      x = table.loc[i]
      acc_id = x["Num_Acc"]
      veh_id = x["num_veh"]
      try:
        vehicule = vehicules[acc_id][veh_id]
      except KeyError:
        vehicule = None
      personne = Personne(gravite=x["grav"], annee_de_naissance=x["an_nais"],
                          sexe=x["sexe"], action_pieton=x["actp"],
                          existence_eqpt_secu=str(x["secu"])[0],
                          utilisation_eqpt_secu=str(x["secu"])[1],
                          localisation=x["locp"], categorie_usager=x["catu"],
                          motif_trajet=x["trajet"],
                          place=x["place"], vehicule=vehicule)
      accidents[acc_id].addPersonne(personne)
    except ValueError:
      continue

  return accidents


def create_RDF(accidents):

  # Create graph
  store = Graph()
  
  #Bind prefixes to namespaces
  store.namespace_manager.bind('dbp',
                               dbp, override = True )
  store.namespace_manager.bind('dbc',
                               dbc, override = True )
  store.namespace_manager.bind('geo',
                               geo, override = True )
  store.namespace_manager.bind('ex',
                               ex, override = True )
  store.namespace_manager.bind('igeo',
                               igeo, override = True )
  store.namespace_manager.bind('foaf',
                               FOAF, override = True )
                                         
                                         
                                         
  # Schema
  store.add((ex.RoadAccident, RDF.type, RDFS.Class))
  store.add((ex.RoadAccident, RDFS.subClassOf, dbc.Event))
  store.add((ex.Commune, RDFS.domain, ex.RoadAccident))
  store.add((ex.Commune, RDFS.range, igeo.Commune))
  store.add((ex.date_time, RDFS.domain, ex.RoadAccident))
  store.add((ex.date_time, RDFS.range, XSD.dateTime))
  store.add((ex.intersection, RDFS.domain, ex.RoadAccident))
  store.add((ex.intersection, RDFS.range, XSD.string))
  store.add((ex.light, RDFS.domain, ex.RoadAccident))
  store.add((ex.light, RDFS.range, XSD.string))
  store.add((ex.weather, RDFS.domain, ex.RoadAccident))
  store.add((ex.weather, RDFS.range, XSD.string))
  store.add((ex.involves, RDFS.domain, ex.RoadAccident))
  store.add((ex.involves, RDFS.range, FOAF.Person))
  
  store.add((ex.Vehicule, RDF.type, RDFS.Class))
  store.add((ex.Vehicule, RDFS.subClassOf, dbc.MeanOfTransportation))
  store.add((ex.sens, RDFS.domain, ex.Vehicule))
  store.add((ex.sens, RDFS.range, XSD.string))
  store.add((ex.categoryV, RDFS.domain, ex.Vehicule))
  store.add((ex.categoryV, RDFS.range, XSD.string))
  store.add((ex.obstacleFixed, RDFS.domain, ex.Vehicule))
  store.add((ex.obstacleFixed, RDFS.range, XSD.string))
  store.add((ex.obstacleMobile, RDFS.domain, ex.Vehicule))
  store.add((ex.obstacleMobile, RDFS.range, XSD.string))
  store.add((ex.impact, RDFS.domain, ex.Vehicule))
  store.add((ex.impact, RDFS.range, XSD.string))
  store.add((ex.maneuver, RDFS.domain, ex.Vehicule))
  store.add((ex.maneuver, RDFS.range, XSD.string))
  
  store.add((dbc.Road, RDF.type, RDFS.Class))
  store.add((ex.catr, RDFS.domain, ex.RoadAccident))
  store.add((ex.catr, RDFS.range, XSD.string))
  store.add((ex.layout, RDFS.domain, ex.RoadAccident))
  store.add((ex.layout, RDFS.range, XSD.string))
  store.add((ex.profile, RDFS.domain, ex.RoadAccident))
  store.add((ex.profile, RDFS.range, XSD.string))
  store.add((ex.circulation, RDFS.domain, ex.RoadAccident))
  store.add((ex.circulation, RDFS.range, XSD.string))
  store.add((ex.centralWidth, RDFS.domain, ex.RoadAccident))
  store.add((ex.centralWidth, RDFS.range, XSD.double))
  
  store.add((igeo.Commune, RDF.type, RDFS.Class))
  
  store.add((FOAF.Person, RDF.type, RDFS.Class))
  store.add((ex.gravity, RDFS.domain, FOAF.Preson))
  store.add((ex.gravity, RDFS.range, XSD.string))
  store.add((ex.birthYear, RDFS.domain, FOAF.Preson))
  store.add((ex.birthYear, RDFS.range, XSD.integer))
  store.add((ex.action, RDFS.domain, FOAF.Preson))
  store.add((ex.action, RDFS.range, XSD.string))
  store.add((ex.gravity, RDFS.domain, FOAF.Preson))
  store.add((ex.gravity, RDFS.range, XSD.string))
  store.add((ex.securityExistence, RDFS.domain, FOAF.Preson))
  store.add((ex.securityExistence, RDFS.range, XSD.string))
  store.add((ex.securityUse, RDFS.domain, FOAF.Preson))
  store.add((ex.securityUse, RDFS.range, XSD.string))
  store.add((ex.localisation, RDFS.domain, FOAF.Preson))
  store.add((ex.localisation, RDFS.range, XSD.string))
  store.add((ex.category, RDFS.domain, FOAF.Preson))
  store.add((ex.category, RDFS.range, XSD.string))
  store.add((ex.reason, RDFS.domain, FOAF.Preson))
  store.add((ex.reason, RDFS.range, XSD.string))
  store.add((ex.gravity, RDFS.domain, FOAF.Preson))
  store.add((ex.gravity, RDFS.range, XSD.string))
  store.add((ex.vehicule, RDFS.domain, FOAF.Preson))
  store.add((ex.vehicule, RDFS.range, ex.Vehicule))

  # Fill graph
  for acc_id, accident in accidents.items():
    accident.addToRDF(store)

  # Serialize as Turtle and write in a .ttl file
  with open("accidents.ttl", 'w') as f:
    f.write(store.serialize(format="turtle").decode("utf-8"))


# Main
create_RDF(create_classes())
